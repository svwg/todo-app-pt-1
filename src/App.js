import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };

  submitHandelertodos = (event) => {
    if (event.key === "Enter") {
      let otherTodos = [...this.state.todos];
      event.preventDefault();
      let otherTodo = {
        userId: 1,
        id: Math.floor(Math.random() * 100),
        title: event.target.value,
        completed: false,
      };
      otherTodos.push(otherTodo);
      this.setState({
        todos: otherTodos,
      });
        event.target.value= ""
      
    }
  };

  deleteTodo = (id) => {
    let otherTodos = this.state.todos.filter((todo) => {
      return todo.id !== id;
    });
    this.setState({
      ...this.state,
      todos: otherTodos,
    });
  };

  handleDelete = () => {
    this.setState({
      todos: this.state.todos.filter((todo) => todo.completed === false),
    });
  };

  completedTodos = (event, id) => {
    let newTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      } else {
        return todo;
      }
    });
    this.setState({ todos: newTodos });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            type="text"
            onKeyDown={this.submitHandelertodos}
          />
        </header>
        <TodoList todos={this.state.todos} deleteTodo={this.deleteTodo} completedTodos={this.completedTodos} />
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.filter((todo) => todo.completed === false).length}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDelete}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.completedTodos}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.deleteTodo} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              deleteTodo={(event) => this.props.deleteTodo(todo.id)}
              completedTodos={(event) => this.props.completedTodos(event, todo.id)}
              //id={todo.id}
              key={todo.id}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
